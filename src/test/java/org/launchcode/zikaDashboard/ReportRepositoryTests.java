package org.launchcode.zikaDashboard;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.launchcode.zikaDashboard.features.WktHelper;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@IntegrationTestConfig

public class ReportRepositoryTests {

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    public void testGetAllReports() {

        entityManager.flush();

        List<Report> foundReport = reportRepository.findAll();

        assertEquals(254, foundReport.size());

    }

}
