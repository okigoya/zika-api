package org.launchcode.zikaDashboard;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.controllers.ReportController;
import org.launchcode.zikaDashboard.data.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportControllerTest {

    @Autowired
    private ReportController reportController;

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetReports() throws Exception {
        this.mockMvc.perform(get("/report/")).andExpect(status().isOk());
    }


}
