package org.launchcode.zikaDashboard;

import com.vividsolutions.jts.geom.Geometry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zikaDashboard.features.WktHelper;
import org.launchcode.zikaDashboard.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityManager;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@TestPropertySource(locations = "classpath:application-test.properties")
@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EntityManager entityManager;

    @Test
    public void testReport() {
        Report report = new Report("1/1/2019", WktHelper.wktToGeometry("POINT(-62.668875 -10.626234)"), "Brazil-Rondonia",  "state",  "zika_reported", "BR0011", "NA", "NA", 618, "cases");

        entityManager.persist(report);
        entityManager.flush();

        assertEquals("1/1/2019", report.getReportDate());

    }
}
